/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `npm run dev` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `npm run deploy` to publish your worker
 *
 * Bind resources to your worker in `wrangler.toml`. After adding bindings, a type definition for the
 * `Env` object can be regenerated with `npm run cf-typegen`.
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

addEventListener('fetch', (event) => {
	event.respondWith(handleRequest(event));
});

interface ReplacementRule {
	path: string;
	replacements: { [key: string]: string };
	redirect?: string;
	data?: (request: Request) => Response;
}

const hostToPromotionalCode: { [key: string]: string } = {
	'pwa-page-kassadin.floozy-team.workers.dev': 'pwa01'
};
// 定义替换规则
const replacementRules: ReplacementRule[] = [
	{
		path: '/8576632294/dns.json',
		replacements: {
			'2541728988-ps79boju.bttzs.com': `api-pwa.relaydns.me`
		}
	},
	{
		path: '/8576632294/app.webmanifest',
		replacements: {
			'Loucura de cassino': 'Pejuang kasino',
			'#15CE01': '#2c2e34',
			'#FFAB2C': '#24262b',
			'.\\/roibest': 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com/8576632294/roibest',
			'webp': 'png',
			'screenshot4': 'screenshot1',
			'474x1024': '624x351'
		}
	},
	{
		path: '/8576632294/roibest/icons/favicon.png',
		redirect: 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com',
		replacements: {}
	},
	{
		path: '/pixgif/r9.gif',
		redirect: 'https://8.8.8.8',
		replacements: {}
	},
	{
		path: '/init/config',
		replacements: {
		},
		data(request: Request): Response {
			return new Response(JSON.stringify({
				"code": 0,
				"msg": "success",
				"data": {
					"id": 8233,
					"app_name": "Pejuang kasino",
					"app_icon": "oss_upload/202409/02/290b32dc531863204d83b098170e775266d5a25b43afc.png",
					"company_name": "",
					"app_comments": 0,
					"app_score": "0.00",
					"app_age_limit": 0,
					"app_desc": "",
					"pic_list": "",
					"status": 0,
					"package_name": "undefined",
					"package_link": "",
					"domain": "pejuangkasino.com",
					"creator": 430,
					"project_id": "8576632294",
					"created_at": "2024-08-28 09:05:26",
					"updated_at": "2024-09-03 10:32:32",
					"deleted_at": null,
					"version": 0,
					"notes": "BR005 0209",
					"remark": "",
					"package_status": 2,
					"domain_id": 10016217,
					"theme_color": "#24262b",
					"background_color": "#2c2e34",
					"feishu_url": "",
					"current_language_code": "",
					"package_type": 2,
					"telegram_url": "",
					"screen_way": 0,
					"all_screen": 0,
					"pre_load": 1,
					"default_menu": 0,
					"hidden": 0,
					"all_click": 1,
					"language_json": {
						"id": {
							"id": 21541,
							"project_id": "8576632294",
							"language_code": "id",
							"created_at": "2024-09-03 10:32:30",
							"updated_at": "2024-09-03 10:32:30",
							"deleted_at": null,
							"app_name": "Pejuang kasino",
							"app_icon": "https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com/oss_upload/202409/02/290b32dc531863204d83b098170e775266d5a25b43afc.png",
							"company_name": "Pejuang Studio",
							"app_comments": 35000,
							"app_age_limit": 0,
							"app_desc": "Bonus sambutan besar menanti Anda!\n" +
								"\n" +
								"Unduh Slots Crush 2024 - casino slots free sekarang dan dapatkan:\n" +
								"\n" +
								"Bonus sambutan slot offline yang mengesankan sebesar 700.000 koin dan banyak putaran gratis!\n" +
								"Slot online baru setiap minggu.",
							"pic_list": [
								"https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com/oss_upload/202408/30/7e4a4612ecd4bb500e8f667f6e2e425b66d20e87cb39b.png",
								"https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com/oss_upload/202408/30/cb17712637af45141dcca044c830d38d66d20e8bf2a63.png",
								"https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com/oss_upload/202408/30/87ca261b0e996d8afece830a6695a1b666d20e9cb8dd2.png",
								"https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com/oss_upload/202408/30/b786d0babaf8426133dccd18bf0f5c6a66d20ea09cfdc.png",
								"https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com/oss_upload/202408/30/cb17712637af45141dcca044c830d38d66d20e8bf2a63.png"
							],
							"app_labels": "Kasino, slot, judi, taruhan, kasino sah, kasino menguntungkan, lotre, rupiah, jackpot, crash, roulette",
							"app_score_detail": {
								"1": 0,
								"2": 0,
								"3": 0,
								"4": 1,
								"5": 10
							},
							"app_comment_list": [
								{
									"name": "kocok SP 991",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/81adde1473d9900cee8896a2a207cda166cee7dc3b470.jpg",
									"score": "5",
									"comment": "Pembayaran cepat, saya sangat menyukainya"
								},
								{
									"name": "nyonyacoca 0192",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/4bde224e5b3c8ba4bf1980c5efd55fdc66cee7ef87f68.jpg",
									"score": "5",
									"comment": "Saya tidak percaya saya memenangkan R$21.000 hari ini dengan bermain di Bra365, sungguh luar biasa!"
								},
								{
									"name": "jogobr 12",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/ed883be73af19be58f460d3accaa5efe66cee8034623a.jpg",
									"score": "5",
									"comment": "Permainan kasino terbaik di Brasil, saya menang berkali-kali di sini"
								},
								{
									"name": "brilian 569",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/5983c0721f4221dea6752a09f2f1369e66cee8127e479.jpg",
									"score": "5",
									"comment": "Aku menang berkali-kali di sini hahaha, aku cinta kamu"
								},
								{
									"name": "Mesinadraen",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/6e24980f67ae6c85f682ab7bbe4fd9c466cee825436d2.png",
									"score": "5",
									"comment": "Saya memenangkan R$16000 di FORTUNE DRAGON dan dalam 3 menit uang sudah masuk ke akun PIX saya, dengan sangat cepat."
								},
								{
									"name": "Kingjogozip",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/dcc709c084f8000d434e6a46caecd43e66cee8311c035.jpg",
									"score": "5",
									"comment": "Saya suka aplikasi ini, aplikasi terbaik yang pernah saya mainkan"
								},
								{
									"name": "selena PL 909",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/ac9141f55311ef9481d19f9e95d9902e66cee83a5d1fb.jpg",
									"score": "5",
									"comment": "Sangat terkesan dengan permainan dan kecepatan pembayaran Bra365"
								},
								{
									"name": "gadis baik hati SP",
									"avatar": "https://static.roibest.com/oss_upload/202408/28/456a4c2defd0a3558ffad49e37daf0d766cee84430fc3.jpg",
									"score": "5",
									"comment": "Saya memenangkan R$ 13000 pada naga keberuntungan hari ini, haha"
								}
							],
							"app_download": 22555666,
							"theme_color": "",
							"background_color": "",
							"app_score": "4.9"
						}
					}
				}
			}),{
				headers: {
					'content-type': 'application/json',
					'access-control-allow-credentials': 'true',
					'access-control-allow-origin': '*',
					'access-control-allow-methods': 'GET, POST, PUT, DELETE, OPTIONS',
					'access-control-allow-headers': 'Content-Type, Authorization, X-Requested-With'
				}
			})
		}
	},
	{
		path: '/roibest-res-new/roibest-assets/roibest-assets/js/install-f7490a535010bc12d209.js',
		replacements: {
			'https://static.roibest.com': 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com'
		}
	},
	{
		path: '/8576632294/8576632294_content.html',
		replacements: {
			'https://static.roibest.com': 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com'
		},
		redirect: 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com'
	},
	{
		path: '/8576632294/__roibest_scan.html',
		replacements: {
			'https://static.roibest.com': 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com'
		},
		redirect: 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com'
	},
	{
		path: '/8576632294/index.html',
		replacements: {
			'https://static.roibest.com': 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com'
		},
		redirect: 'https://gaia4-pub.oss-ap-southeast-5.aliyuncs.com'
	},
	{
		data(request: Request): Response {
			const url = new URL(request.url);
			return new Response(JSON.stringify({
				'code': 0,
				'msg': 'success',
				'data': {
					'data': {
						'id': 24428,
						'creator': 430,
						'project_id': '8576632294',
						'channel_id': 4,
						'app_id': 8233,
						'account_id': 20117,
						'third_account_id': 0,
						'third_channel_id': 0,
						'url': `${url.origin}/8576632294/8576632294_content.html?channel_id=4&promote_url_id=5215390555&invite_code=`,
						'note': 'BR003 2808 ACHAO',
						'created_at': '2024-08-28 09:06:47',
						'updated_at': '2024-08-28 09:06:47',
						'deleted_at': null,
						'promote_url_id': '5215390555',
						'domain_id': 10016217,
						'package_addr': `https://pejuangkasino.com/?spreadCode=${hostToPromotionalCode[url.hostname]}`,
						'ios_url': '',
						'confusion_code': 'nca9***u0',
						'hidden': 0,
						'suffix': 'content',
						'changeable': 0
					}
				}
			}), {
				headers: {
					'content-type': 'application/json',
					'access-control-allow-credentials': 'true',
					'access-control-allow-origin': '*',
					'access-control-allow-methods': 'GET, POST, PUT, DELETE, OPTIONS',
					'access-control-allow-headers': 'Content-Type, Authorization, X-Requested-With'
				}
			});
		},
		path: '/get/promoteUrlInfo',
		replacements: {}
	},
	{
		path:'/roibest-res-new/roibest-assets/roibest-assets/js/locale6-a24b3511de9d1c3ad57d.js'
		,replacements:{
			'Kami mengumpulkan ulasan <span>objektif</span> untuk app ini':'Kami mengumpulkan tinjauan<span>tujuan</span> untuk app ini'
		}
	}
];
const agencyRules: { [key: string]: string } = {
	'api-pwa.relaydns.me': 'https://2541728988-ps79boju.bttzs.com'
};

async function handleRequest(event: FetchEvent): Promise<Response> {
	const request = event.request;
	// 获取请求的 URL
	const url = new URL(request.url);

	if (url.pathname === '/') {
		// 重定向
		return Response.redirect(`${url.origin}/8576632294/8576632294_content.html${url.search}`, 302);
	}
	// 检查是否预制结果data
	if (replacementRules.some(rule => url.pathname.startsWith(rule.path) && rule.data)) {
		const rule = replacementRules.find(rule => url.pathname.startsWith(rule.path));
		return rule?.data!(request)!;
	}

	// 定义要代理的目标网站
	let targetHost = agencyRules[url.hostname] || 'https://www.googlestorebr.com';

	// 检查是否有重定向
	if (replacementRules.some(rule => url.pathname.startsWith(rule.path) && rule.redirect)) {
		targetHost = replacementRules.find(rule => url.pathname.startsWith(rule.path))?.redirect!;
	}

	// 构建代理 URL
	const proxyUrl = new URL(targetHost + url.pathname + url.search);
	// 创建新的请求对象
	const proxyRequest = new Request(proxyUrl.toString(), {
		method: request.method,
		headers: request.headers,
		body: request.body,
		redirect: 'manual'
	});

	// 获取代理响应
	let response = await fetch(proxyRequest);

	const contentType = response.headers.get('Content-Type') || '';

	// 检查路径并进行内容替换
	for (const rule of replacementRules) {
		if (url.pathname.startsWith(rule.path)) {
			if (contentType.includes('text')) {
				let text = await response.text();

				// 批量替换内容
				for (const [oldContent, newContent] of Object.entries(rule.replacements)) {
					text = text.replaceAll(oldContent, newContent);
				}

				// 创建新的响应对象
				response = new Response(text, {
					status: response.status,
					statusText: response.statusText,
					headers: {
						...response.headers,
						'content-type': request.headers.get('content-type') || response.headers.get('content-type') || 'text/html'
					}
				});
			} else if (contentType.includes('application/octet-stream')) {
				let buffer = await response.arrayBuffer();
				let text = new TextDecoder().decode(buffer);
				// 批量替换内容
				for (const [oldContent, newContent] of Object.entries(rule.replacements)) {
					text = text.replaceAll(oldContent, newContent);
				}
				// 将文本编码为 ArrayBuffer
				buffer = new TextEncoder().encode(text).buffer;
				// 创建新的响应对象
				response = new Response(buffer, {
					status: response.status,
					statusText: response.statusText,
					headers: response.headers
				});
			} else if (contentType.includes('application/json')) {
				let json = await response.json();
				// 批量替换内容
				for (const [oldContent, newContent] of Object.entries(rule.replacements)) {
					json = JSON.parse(JSON.stringify(json).replaceAll(oldContent, newContent));
				}
				// 创建新的响应对象
				response = new Response(JSON.stringify(json), {
					status: response.status,
					statusText: response.statusText,
					headers: response.headers
				});
			} else if (contentType.includes('application/javascript')) {
				let text = await response.text();
				// 批量替换内容
				for (const [oldContent, newContent] of Object.entries(rule.replacements)) {
					text = text.replaceAll(oldContent, newContent);
				}
				// 创建新的响应对象
				response = new Response(text, {
					status: response.status,
					statusText: response.statusText,
					headers: response.headers
				});
			}
			break;
		}
	}


	return response;
}

